package com.devcamp.api.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.api.repository.IVoucherRepository;

import com.devcamp.api.model.*;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CVoucherController {
	@Autowired
	IVoucherRepository pVoucherRepository;

	@GetMapping("/vouchers")
	public ResponseEntity<List<CVoucher>> getAllVoucher() {
		try {
			List<CVoucher> pVouchers = new ArrayList<CVoucher>();
			pVoucherRepository.findAll().forEach(pVouchers::add);
			return new ResponseEntity<>(pVouchers, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
